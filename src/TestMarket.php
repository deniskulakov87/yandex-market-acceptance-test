<?php

namespace Facebook\WebDriver;
use PHPUnit_Framework_TestCase;

require_once('pages/MarketMainPage.php');
require_once('pages/LoginPage.php');
require_once('pages/ProductPage.php');
require_once('pages/CartPage.php');
require_once('pages/SettingsPage.php');
require_once('pages/CheckoutPage.php');
require_once('vendor/autoload.php');

class TestMarket extends PHPUnit_Framework_TestCase
{
    private $searchTerm = 'iphone 7 128Gb';
    private $searchProduct = 'Apple iPhone 7 128Gb';

    public function testMarket()
    {
        MarketMainPage::open();
        $this->screen("step_1");

        MarketMainPage::goToLoginPage();
        LoginPage::login();
        $this->screen("step_2");

        MarketMainPage::findProduct($this->searchTerm);
        $this->screen("step_3");

        $index = MarketMainPage::checkIfProductExistsInResults($this->searchProduct);
        self::assertNotEquals(0, $index, 'Product not found');
        $this->screen("step_4");

        MarketMainPage::enterProductPage($index);
        $this->screen("step_5");

        ProductPage::chooseTheCheapestProduct();
        $this->screen("step_6");

        ProductPage::addChosenProductToTheCart();
        $this->screen("step_7");

        MarketMainPage::open();
        MarketMainPage::goToSettings();
        SettingsPage::getEmailAndName();

        MarketMainPage::open();
        MarketMainPage::goToTheCart();
        $numberOfProducts = CartPage::checkNumberOfProductsAndGetProductName();
        self::assertEquals(1, $numberOfProducts, "Cart contains wrong number of products");
        $this->screen("step_8");

        CartPage::goToCheckoutPage();
        $this->screen("step_9");

        CheckoutPage::getData();
        self::assertEquals(SettingsPage::$email, CheckoutPage::$email, "Problem with email");
        self::assertEquals(SettingsPage::$name, CheckoutPage::$name, "Problem with name");
        self::assertEquals(CartPage::$product, CheckoutPage::$product, "Problem with email");
        $this->screen("step_10");


        CheckoutPage::DeleteProduct();
        MarketMainPage::goToTheCart();
        $this->screen("step_11");

        MarketMainPage::logout();
        $this->screen("step_12");
    }

    public function tearDown()
    {
        Driver::Instance()->quit();
    }

    public function screen($name) {
        Driver::Instance()->takeScreenshot(__DIR__ . "/". $name .".jpg");
    }
}
?>


