<?php

namespace Facebook\WebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;

final class Driver
{
    public $webDriver;

    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = RemoteWebDriver::create('http://localhost:4444/wd/hub', DesiredCapabilities::chrome());
            $inst->manage()->timeouts()->pageLoadTimeout(20);
            $inst->manage()->timeouts()->implicitlyWait(20);
        }
        return $inst;
    }

    private function __construct()
    {

    }
}