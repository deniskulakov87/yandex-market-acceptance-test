<?php

namespace Facebook\WebDriver;
require_once('./Driver.php');

class MarketMainPage
{
    private static $loginButtonXPath = '/html/body/div/div[1]/noindex/div/div/div[3]/div/div[2]/div[1]/div/a';
    private static $searchStringClassName = 'input__control';
    private static $searchButtonXPath = '/html/body/div/div[1]/noindex/div/div/div[2]/div/div[1]/form/span[2]/button';
    private static $searchResultsXPath = '//*[@class="snippet-card__header-text"]';
    private static $gotoCartButtonXPath = '(//*[@class="header2-menu__text"])[3]';
    private static $mainPageURL = 'https://market.yandex.ru';
    private static $logoXPath = '(//*[@href="https://passport.yandex.ru/passport?mode=passport"])[2]';
    private static $settingsMenuItemXPath = '//*[@href="/my/settings?track=menu&from="]';
    private static $logoutMenuItemXPath = '/html/body/div[1]/div[1]/noindex/div/div/div[3]/div/div[2]/div[2]/div/ul[2]/li[6]/a';

    public static function open() {
        Driver::Instance()->get(self::$mainPageURL);
    }

    public static function goToLoginPage() {
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$loginButtonXPath))->click();
    }

    public static function findProduct($searchTerm) {
        Driver::Instance()->findElement(WebDriverBy::className(self::$searchStringClassName))->sendKeys($searchTerm);
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$searchButtonXPath))->click();
    }

    public static function checkIfProductExistsInResults($searchProduct)
    {
        $index = 0;
        foreach (Driver::Instance()->findElements(WebDriverBy::xpath(self::$searchResultsXPath)) as $element) {
            $index += 1;
            if ($element->getText() == $searchProduct) {
                return $index;
            }
        }
        return 0;
    }

    public static function enterProductPage($index)
    {
        Driver::Instance()->findElement(WebDriverBy::xpath("(".self::$searchResultsXPath.")"."[".$index."]"))->click();
    }

    public static function goToTheCart()
    {
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$gotoCartButtonXPath))->click();
    }

    public static function goToSettings()
    {
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$logoXPath))->click();
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$settingsMenuItemXPath))->click();
    }

    public static function logout()
    {
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$logoXPath))->click();
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$logoutMenuItemXPath))->click();
        Driver::Instance()->navigate()->refresh();

    }

}
?>


