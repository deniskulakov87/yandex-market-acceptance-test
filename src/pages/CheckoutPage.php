<?php

namespace Facebook\WebDriver;
require_once('./Driver.php');

class CheckoutPage
{
    public static $email;
    public static $name;
    public static $product;

    private static $emailName = 'user-email';
    private static $nameName = 'user-name';
    private static $productClassName = 'n-checkout-offer__item-name';
    private static $deleteButtonXPath = '/html/body/div[1]/div[2]/div[1]/div[2]/div[1]/div/div/div/div[4]/div/div/div/div/div/div[6]/div';

    public static function getData()
    {
        self::$email = Driver::Instance()->findElement(WebDriverBy::name(self::$emailName))->getAttribute("value");
        self::$name = Driver::Instance()->findElement(WebDriverBy::name(self::$nameName))->getAttribute("value");
        self::$product = Driver::Instance()->findElement(WebDriverBy::className(self::$productClassName))->getText();
    }

    public static function DeleteProduct()
    {
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$deleteButtonXPath))->click();
    }
}