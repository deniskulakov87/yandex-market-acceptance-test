<?php

namespace Facebook\WebDriver;
require_once('./Driver.php');

class SettingsPage
{
    public static $email;
    public static $name;
    private static $emailClassName = 'n-settings-email-select__selected-item';
    private static $nameClassName = 'settings-list__value';

    public static function getEmailAndName()
    {
        self::$email = Driver::Instance()->findElement(WebDriverBy::className(self::$emailClassName))->getText();
        self::$name = Driver::Instance()->findElement(WebDriverBy::className(self::$nameClassName))->getText();

//        print(self::$email. "   ");
//        print(self::$name. "   ");
    }
}