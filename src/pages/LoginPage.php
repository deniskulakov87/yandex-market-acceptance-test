<?php

namespace Facebook\WebDriver;
require_once('./Driver.php');

class LoginPage
{
    private static $userNameInputXPath = '//*[@name="login"]';
    private static $pwdInputXPath = '//*[@name="passwd"]';
    private static $loginButtonXPath = '/html/body/div[2]/div[1]/div/div/div/div[2]/form/div[4]/div/button';

    private static $userName = '<userName>';
    private static $password = '<password>';

    public static function login() {
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$userNameInputXPath))->sendKeys(self::$userName);
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$pwdInputXPath))->sendKeys(self::$password);
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$loginButtonXPath))->click();
    }
}
?>


