<?php

namespace Facebook\WebDriver;
require_once('./Driver.php');

class ProductPage
{
    private static $priceTabXPath = '/html/body/div[1]/div[4]/div/div[1]/div/ul/li[3]/a';
    private static $sortByPriceLabelXPath = '/html/body/div[1]/div[5]/div[1]/div[2]/div[1]/div[1]/div[3]/a';
    private static $addToCartButtonInChosenProductPopUpXPath = '/html/body/div[3]/div/div/div/div/div[2]/div[1]/div[2]/a';
    private static $topSearchResultXPath = '(//*[@class="snippet-card__header-text"])[1]';


    public static function enterProductPage($index)
    {
        Driver::Instance()->findElement(WebDriverBy::xpath("(".self::$searchResultsXPath.")"."[".$index."]"))->click();
    }

    public static function chooseTheCheapestProduct()
    {
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$priceTabXPath))->click();
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$sortByPriceLabelXPath))->click();


        //Driver::Instance()->wait(30, 250)->until(WebDriverExpectedCondition::elementToBeClickable(WebDriverBy::xpath(self::$topSearchResultXPath)));
        Driver::Instance()->navigate()->refresh();
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$topSearchResultXPath))->click();

    }

    public static function addChosenProductToTheCart()
    {
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$addToCartButtonInChosenProductPopUpXPath))->click();
    }

}
?>


