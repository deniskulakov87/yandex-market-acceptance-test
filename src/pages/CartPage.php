<?php

namespace Facebook\WebDriver;
require_once('./Driver.php');

class CartPage
{
    private static $resultXPath = '/html/body/div[1]/div[3]/div/div[1]/div/form/div/div[1]';
    private static $goToCheckoutPageButtonXPath = '(//*[@role="button"])[4]';

    public static $product;
    private static $productXPath = '/html/body/div[1]/div[3]/div/div[1]/div/ul/li/div[1]/h4/a';

    public static function checkNumberOfProductsAndGetProductName()
    {
        $text =  Driver::Instance()->findElement(WebDriverBy::xpath(self::$resultXPath))->getText();
        self::$product = Driver::Instance()->findElement(WebDriverBy::xpath(self::$productXPath))->getText();
        return trim(substr(substr($text, strlen("Итого: ")), 0, 2));
    }

    public static function goToCheckoutPage()
    {
        Driver::Instance()->findElement(WebDriverBy::xpath(self::$goToCheckoutPageButtonXPath))->click();
    }

}
?>


