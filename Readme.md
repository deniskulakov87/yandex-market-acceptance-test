# Acceptance-тест яндекс.маркет
Тест написан на PHP с использованием [Facebook webdriver](https://github.com/facebook/php-webdriver) и [PHPUnit](https://phpunit.de/) с применением паттерна Page Object Model. 

Тест выполняет следующую последовательность действий:
1. Перейти на market.yandex.ru
2. Авторизоваться с использованием логина и пароля
3. Осуществить поиск по строке “iphone 7 128Gb”
4. Проверить что в результатах поиска есть модель 'Apple iPhone 7 128Gb'
5. Перейти на страницу модели
6. Выбрать самое дешевое предложение
7. Добавить его в корзину
8. Перейти в корзину и убедиться что в корзине 1 добавленный товар
9. Нажать “Оформить заказ” и перейти на страницу оформления заказа
10. Убедиться что в форме заказа находится 1 товар из корзины и подставлены корректные 11. данные покупателя, взятые из настроек Маркета: e-mail, Имя и Фамилия
11. Очистить корзину
12. Выйти из аккаунта

После каждого шага сохраняется скриншот.

### Установка
Вам потребуется selenium сервер для запуска клиента - selenium-server-standalone-#.jar. Его можно взять [здесь](http://selenium-release.storage.googleapis.com/index.html).
Для тестирования в Chrome потребуется соответствующий драйвер. Его можно взять [здесь](https://chromedriver.storage.googleapis.com/index.html?path=2.27/).
Оба этих файла можно также найти в папке supporting files.

1. Скопировать себе папку src, перейти из терминала в нее;
2. Установить композер:
```sh
curl -sS https://getcomposer.org/installer | php
```

3. Установить требуемые библиотеки через composer:
```sh
php composer.phar install
```

4. В файле pages/LoginPage.php необходимо установить свои логин и пароль от маркета:
```sh
private static $userName = '<username>';
private static $password = '<password>';
```

### Запуск
1. Запустить selenium сервер (при многократном использовании рекомендуется строку запуска поместить в .bash_profile). Для запуска может потребоваться JDK:
```sh
java -jar selenium-server-standalone-#.jar -Dwebdriver.chrome.driver=path_to_chromedriver
Например:
java -jar /usr/local/bin/selenium-server-standalone-2.53.1.jar -Dwebdriver.chrome.driver=/Users/deniskulakov/Downloads/chromedriver
```

2. Запустить тест:
```sh
phpunit TestMarket.php
или если phpunit не добавлен в .bash_profile:
vendor/phpunit/phpunit/phpunit TestMarket.php
```

После выполения теста в консоли появитя результат выполнения в виде:
```sh
.                                                        1 / 1 (100%)
Time: 1.21 minutes, Memory: 6.25MB
OK (1 test, 5 assertions)
```

